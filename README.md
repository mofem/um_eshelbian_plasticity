# Eshelbian plasticity module # {#um_eshelbian_plasticity_readme}

## Installing MoFEM

Follow installation of MoFEM with the source code described 
[here](http://mofem.eng.gla.ac.uk/mofem/html/install_spack.html). 

### Cloning module source code

MoFEM core source code is basic installation. Source code of particular 
projected is located in stand alone repository. This module has source code
located in bitbucket repository.  

```
cd $HOME/mofem-cephas/mofem/users_modules
git clone git@bitbucket.org:mofem/um_eshelbian_plasticity.git eshelbian_plasticity
```
