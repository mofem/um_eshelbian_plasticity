/**
 * \file EshelbianPlasticity.cpp
 * \brief Implementation of automatic differentiation
 */

#include <MoFEM.hpp>
using namespace MoFEM;

#include <BasicFiniteElements.hpp>

#include <EshelbianPlasticity.hpp>
#include <boost/math/constants/constants.hpp>

constexpr double third = boost::math::constants::third<double>();

namespace EshelbianPlasticity {

struct OpHMHH : public OpJacobian {
  OpHMHH(const std::string &field_name, const int tag, const bool eval_rhs,
         const bool eval_lhs, boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
         boost::shared_ptr<PhysicalEquations> &physics_ptr)
      : OpJacobian(field_name, tag, eval_rhs, eval_lhs, data_ptr, physics_ptr) {
  }

  MoFEMErrorCode evaluateRhs(EntData &data);
  MoFEMErrorCode evaluateLhs(EntData &data);
};

MoFEMErrorCode OpHMHH::evaluateRhs(EntData &ent_data) {
  MoFEMFunctionBegin;
  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;

  const auto nb_integration_pts = ent_data.getN().size1();
  auto iu = getFTensor2SymmetricFromMat<3>(dataAtPts->streachTensorAtPts);
  // FIXME: that should work with material streach
  auto iG = getFTensor2FromMat<3, 3>(dataAtPts->GAtPts);

  auto create_data_vec = [nb_integration_pts](auto &v) {
    v.resize(nb_integration_pts, false);
  };
  auto create_data_mat = [nb_integration_pts](auto &m) {
    m.resize(9, nb_integration_pts, false);
  };

  create_data_mat(dataAtPts->PAtPts);
  create_data_mat(dataAtPts->SigmaAtPts);
  create_data_vec(dataAtPts->phiAtPts);
  create_data_mat(dataAtPts->flowAtPts);

  auto r_P = getFTensor2FromMat<3, 3>(dataAtPts->PAtPts);
  auto r_Sigma = getFTensor2FromMat<3, 3>(dataAtPts->SigmaAtPts);
  auto r_phi = getFTensor0FromVec(dataAtPts->phiAtPts);
  auto r_flow = getFTensor2FromMat<3, 3>(dataAtPts->flowAtPts);

  for (unsigned int gg = 0; gg != nb_integration_pts; ++gg) {

    auto t_h = physicsPtr->get_h();
    for (auto ii : {0, 1, 2})
      for (auto jj : {0, 1, 2})
        t_h(ii, jj) = iu(ii, jj);

    physicsPtr->get_H()(i, j) = iG(i, j);
    for (int ii = 0; ii != 3; ++ii)
      physicsPtr->get_H()(ii, ii) += 1;

    // CHKERR physicsPtr->recordTape(tAg, &t_h);
    int r = ::function(tAg, physicsPtr->dependentVariables.size(),
                       physicsPtr->activeVariables.size(),
                       &physicsPtr->activeVariables[0],
                       &physicsPtr->dependentVariables[0]);
    if (r < 0) { // function is locally analytic
      SETERRQ1(PETSC_COMM_SELF, MOFEM_OPERATION_UNSUCCESSFUL,
               "ADOL-C function evaluation with error r = %d", r);
    }

    r_P(i, j) = physicsPtr->get_P()(i, j);
    r_Sigma(i, j) = physicsPtr->get_Sigma()(i, j);
    r_phi = physicsPtr->get_Phi();
    r_flow(i, j) = physicsPtr->get_Flow()(i, j);

    ++iu;
    ++iG;
    ++r_P;
    ++r_Sigma;
    ++r_phi;
    ++r_flow;
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OpHMHH::evaluateLhs(EntData &ent_data) {
  MoFEMFunctionBegin;
  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;
  const int number_of_active_variables = physicsPtr->activeVariables.size();
  const int number_of_dependent_variables =
      physicsPtr->dependentVariables.size();
  std::vector<double *> jac_ptr(number_of_dependent_variables);
  for (unsigned int n = 0; n != number_of_dependent_variables; ++n) {
    jac_ptr[n] =
        &(physicsPtr
              ->dependentVariablesDirevatives[n * number_of_active_variables]);
  }

  const auto nb_integration_pts = ent_data.getN().size1();

  auto create_data_mat = [nb_integration_pts](auto &m) {
    m.resize(9, nb_integration_pts, false);
  };

  auto create_data_ten = [nb_integration_pts](auto &m) {
    m.resize(27, nb_integration_pts, false);
  };

  create_data_ten(dataAtPts->P_dh0);
  create_data_ten(dataAtPts->P_dh1);
  create_data_ten(dataAtPts->P_dh2);
  create_data_ten(dataAtPts->P_dH0);
  create_data_ten(dataAtPts->P_dH1);
  create_data_ten(dataAtPts->P_dH2);
  create_data_ten(dataAtPts->Sigma_dh0);
  create_data_ten(dataAtPts->Sigma_dh1);
  create_data_ten(dataAtPts->Sigma_dh2);
  create_data_ten(dataAtPts->Sigma_dH0);
  create_data_ten(dataAtPts->Sigma_dH1);
  create_data_ten(dataAtPts->Sigma_dH2);
  create_data_mat(dataAtPts->phi_dh);
  create_data_mat(dataAtPts->phi_dH);
  create_data_ten(dataAtPts->Flow_dh0);
  create_data_ten(dataAtPts->Flow_dh1);
  create_data_ten(dataAtPts->Flow_dh2);
  create_data_ten(dataAtPts->Flow_dH0);
  create_data_ten(dataAtPts->Flow_dH1);
  create_data_ten(dataAtPts->Flow_dH2);

  auto iu = getFTensor2SymmetricFromMat<3>(dataAtPts->streachTensorAtPts);
  auto iG = getFTensor2FromMat<3, 3>(dataAtPts->GAtPts);

  auto r_P_dh0 = getFTensor3FromMat(dataAtPts->P_dh0);
  auto r_P_dh1 = getFTensor3FromMat(dataAtPts->P_dh1);
  auto r_P_dh2 = getFTensor3FromMat(dataAtPts->P_dh2);
  auto r_P_dH0 = getFTensor3FromMat(dataAtPts->P_dH0);
  auto r_P_dH1 = getFTensor3FromMat(dataAtPts->P_dH1);
  auto r_P_dH2 = getFTensor3FromMat(dataAtPts->P_dH2);
  auto r_Sigma_dh0 = getFTensor3FromMat(dataAtPts->Sigma_dh0);
  auto r_Sigma_dh1 = getFTensor3FromMat(dataAtPts->Sigma_dh1);
  auto r_Sigma_dh2 = getFTensor3FromMat(dataAtPts->Sigma_dh2);
  auto r_Sigma_dH0 = getFTensor3FromMat(dataAtPts->Sigma_dH0);
  auto r_Sigma_dH1 = getFTensor3FromMat(dataAtPts->Sigma_dH1);
  auto r_Sigma_dH2 = getFTensor3FromMat(dataAtPts->Sigma_dH2);
  auto r_phi_dh = getFTensor2FromMat<3, 3>(dataAtPts->phi_dh);
  auto r_phi_dH = getFTensor2FromMat<3, 3>(dataAtPts->phi_dH);
  auto r_Flow_dh0 = getFTensor3FromMat(dataAtPts->Flow_dh0);
  auto r_Flow_dh1 = getFTensor3FromMat(dataAtPts->Flow_dh1);
  auto r_Flow_dh2 = getFTensor3FromMat(dataAtPts->Flow_dh2);
  auto r_Flow_dH0 = getFTensor3FromMat(dataAtPts->Flow_dH0);
  auto r_Flow_dH1 = getFTensor3FromMat(dataAtPts->Flow_dH1);
  auto r_Flow_dH2 = getFTensor3FromMat(dataAtPts->Flow_dH2);

  for (unsigned int gg = 0; gg != nb_integration_pts; ++gg) {

    auto t_h = physicsPtr->get_h();
    for(auto ii : {0, 1, 2})
      for(auto jj : {0, 1, 2})
        t_h(ii, jj) = iu(ii, jj);

    physicsPtr->get_H()(i, j) = iG(i, j);
    for (int ii = 0; ii != 3; ++ii)
      physicsPtr->get_H()(ii, ii) += 1;

    // play recorder for jacobians
    // CHKERR physicsPtr->recordTape(tAg, &t_h);
    int r = ::jacobian(tAg, number_of_dependent_variables,
                       number_of_active_variables,
                       &physicsPtr->activeVariables[0], &jac_ptr[0]);
    if (r < 0) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_OPERATION_UNSUCCESSFUL,
              "ADOL-C function evaluation with error");
    }

    r_P_dh0(i, j, k) = physicsPtr->get_P_dh0()(i, j, k);
    r_P_dh1(i, j, k) = physicsPtr->get_P_dh1()(i, j, k);
    r_P_dh2(i, j, k) = physicsPtr->get_P_dh2()(i, j, k);
    r_P_dH0(i, j, k) = physicsPtr->get_P_dH0()(i, j, k);
    r_P_dH1(i, j, k) = physicsPtr->get_P_dH1()(i, j, k);
    r_P_dH2(i, j, k) = physicsPtr->get_P_dH2()(i, j, k);
    r_Sigma_dh0(i, j, k) = physicsPtr->get_Sigma_dh0()(i, j, k);
    r_Sigma_dh1(i, j, k) = physicsPtr->get_Sigma_dh1()(i, j, k);
    r_Sigma_dh2(i, j, k) = physicsPtr->get_Sigma_dh2()(i, j, k);
    r_Sigma_dH0(i, j, k) = physicsPtr->get_Sigma_dH0()(i, j, k);
    r_Sigma_dH1(i, j, k) = physicsPtr->get_Sigma_dH1()(i, j, k);
    r_Sigma_dH2(i, j, k) = physicsPtr->get_Sigma_dH2()(i, j, k);
    r_phi_dh(i, j) = physicsPtr->get_Phi_dh()(i, j);
    r_phi_dH(i, j) = physicsPtr->get_Phi_dH()(i, j);
    r_Flow_dh0(i, j, k) = physicsPtr->get_Flow_dh0()(i, j, k);
    r_Flow_dh1(i, j, k) = physicsPtr->get_Flow_dh1()(i, j, k);
    r_Flow_dh2(i, j, k) = physicsPtr->get_Flow_dh2()(i, j, k);
    r_Flow_dH0(i, j, k) = physicsPtr->get_Flow_dH0()(i, j, k);
    r_Flow_dH1(i, j, k) = physicsPtr->get_Flow_dH1()(i, j, k);
    r_Flow_dH2(i, j, k) = physicsPtr->get_Flow_dH2()(i, j, k);

    ++iu;
    ++iG;
    ++r_P_dh0;
    ++r_P_dh1;
    ++r_P_dh2;
    ++r_P_dH0;
    ++r_P_dH1;
    ++r_P_dH2;
    ++r_Sigma_dh0;
    ++r_Sigma_dh1;
    ++r_Sigma_dh2;
    ++r_Sigma_dH0;
    ++r_Sigma_dH1;
    ++r_Sigma_dH2;
    ++r_phi_dh;
    ++r_phi_dH;
    ++r_Flow_dh0;
    ++r_Flow_dh1;
    ++r_Flow_dh2;
    ++r_Flow_dH0;
    ++r_Flow_dH1;
    ++r_Flow_dH2;
  }
  MoFEMFunctionReturn(0);
}

struct HMHStVenantKirchhoff : public PhysicalEquations {

  static constexpr int numberOfActiveVariables = 9 + 9;
  static constexpr int numberOfDependentVariables = 9 + 9 + 1 + 9;

  HMHStVenantKirchhoff(const double lambda, const double mu,
                       const double sigma_y)
      : PhysicalEquations(numberOfActiveVariables, numberOfDependentVariables),
        lambda(lambda), mu(mu), sigmaY(sigma_y) {}

  MoFEMErrorCode getOptions() {
    MoFEMFunctionBegin;

    double E = 1;
    double nu = 0;

    CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "stvenant_", "", "none");

    CHKERR PetscOptionsScalar("-young_modulus", "Young modulus", "", E, &E,
                              PETSC_NULL);
    CHKERR PetscOptionsScalar("-poisson_ratio", "poisson ratio", "", nu, &nu,
                              PETSC_NULL);
    CHKERR PetscOptionsScalar("-sigmaY", "plastic strain", "", sigmaY, &sigmaY,
                              PETSC_NULL);

    ierr = PetscOptionsEnd();
    CHKERRG(ierr);

    lambda = LAMBDA(E, nu);
    mu = MU(E, nu);

    MoFEMFunctionReturn(0);
  }

  virtual OpJacobian *
  returnOpJacobian(const std::string &field_name, const int tag,
                   const bool eval_rhs, const bool eval_lhs,
                   boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                   boost::shared_ptr<PhysicalEquations> &physics_ptr) {
    return (
        new OpHMHH(field_name, tag, eval_rhs, eval_lhs, data_ptr, physics_ptr));
  }

  double lambda;
  double mu;
  double sigmaY;

  ATensor2 th;
  ATensor2 tH;
  ATensor2 tP;
  ATensor2 tSigma;
  adouble phi;
  adouble s2;
  adouble f;
  adouble energy;

  adouble detH;
  adouble detF;
  adouble trE;
  adouble meanCauchy;
  ATensor2 tInvH;
  ATensor2 tF;
  ATensor2 tInvF;
  ATensor2 tC;
  ATensor2 tE;
  ATensor2 tS;
  ATensor2 tCauchy;
  ATensor2 tDevCauchy;

  ATensor2 tPhi_dDevCauchy;
  ATensor2 tPhi_dCauchy;
  ATensor2 tPhi_dSigma;

  ATensor2 tPulledP;
  ATensor2 tPulledSigma;
  ATensor2 tPulledPhi_dSigma;

  MoFEMErrorCode recordTape(const int tape, DTensor2Ptr *t_h_ptr) {
    MoFEMFunctionBegin;
    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;
    FTensor::Index<'I', 3> I;
    FTensor::Index<'J', 3> J;
    FTensor::Number<0> N0;
    FTensor::Number<1> N1;
    FTensor::Number<2> N2;

    CHKERR getOptions();

    auto ih = get_h();
    auto iH = get_H();
    if (t_h_ptr)
      ih(i, j) = (*t_h_ptr)(i, j);
    else {
      ih(i, j) = 0;
      for (auto ii : {0, 1, 2})
        ih(ii, ii) = 1;
    }

    iH(i, j) = 0;
    for (auto ii : {0, 1, 2})
      iH(ii, ii) = 1;

    auto r_P = get_P();
    auto r_Sigma = get_Sigma();
    double &r_phi = get_PhiRef();
    auto r_Flow = get_Flow();

    enableMinMaxUsingAbs();
    trace_on(tape);

    // Set active variables to ADOL-C
    th(i, j) <<= get_h()(i, j);
    tH(i, j) <<= get_H()(i, j);

    // Deformation gradient
    CHKERR determinantTensor3by3(tH, detH);
    CHKERR invertTensor3by3(tH, detH, tInvH);
    tF(i, I) = th(i, J) * tInvH(J, I);
    CHKERR determinantTensor3by3(tF, detF);
    CHKERR invertTensor3by3(tF, detF, tInvF);

    // Deformation and strain
    tC(I, J) = tF(i, I) * tF(i, J);
    tE(I, J) = tC(I, J);
    tE(N0, N0) -= 1;
    tE(N1, N1) -= 1;
    tE(N2, N2) -= 1;
    tE(I, J) *= 0.5;

    // Energy
    trE = tE(I, I);
    energy = 0.5 * lambda * trE * trE + mu * (tE(I, J) * tE(I, J));

    // Stress Piola II
    tS(I, J) = tE(I, J);
    tS(I, J) *= 2 * mu;
    tS(N0, N0) += lambda * trE;
    tS(N1, N1) += lambda * trE;
    tS(N2, N2) += lambda * trE;
    // Stress Piola I
    tP(i, J) = tF(i, I) * tS(I, J);
    // Stress Cauchy
    tCauchy(i, j) = tP(i, J) * tF(j, J);
    tCauchy(i, j) *= (1 / detF);

    // Stress Eshelby
    tSigma(I, J) = -tF(i, I) * tP(i, J);
    tSigma(N0, N0) += energy;
    tSigma(N1, N1) += energy;
    tSigma(N2, N2) += energy;

    // Deviator Cauchy Stress
    meanCauchy = third * tCauchy(i, i);
    tDevCauchy(i, j) = tCauchy(i, j);
    tDevCauchy(N0, N0) -= meanCauchy;
    tDevCauchy(N1, N1) -= meanCauchy;
    tDevCauchy(N2, N2) -= meanCauchy;

    // Plastic surface

    s2 = tDevCauchy(i, j) * tDevCauchy(i, j); // s:s
    f = sqrt(1.5 * s2);
    phi = f - sigmaY;

    // Flow
    tPhi_dDevCauchy(i, j) = tDevCauchy(i, j);
    tPhi_dDevCauchy(i, j) *= 1.5 / f;
    tPhi_dCauchy(i, j) = tPhi_dDevCauchy(i, j);
    tPhi_dCauchy(N0, N0) -= third * tPhi_dDevCauchy(i, i);
    tPhi_dCauchy(N1, N1) -= third * tPhi_dDevCauchy(i, i);
    tPhi_dCauchy(N2, N2) -= third * tPhi_dDevCauchy(i, i);
    tPhi_dSigma(I, J) = tInvF(I, i) * (tPhi_dCauchy(i, j) * tF(j, J));
    tPhi_dSigma(I, J) *= -(1 / detF);

    // Pull back
    tPulledP(i, J) = tP(i, I) * tInvH(J, I);
    tPulledP(i, J) *= detH;
    tPulledSigma(i, J) = tSigma(i, I) * tInvH(J, I);
    tPulledSigma(i, J) *= detH;
    tPulledPhi_dSigma(i, J) = tPhi_dSigma(i, I) * tInvH(J, I);
    tPulledPhi_dSigma(i, J) *= detH;

    // Set dependent variables to ADOL-C
    tPulledP(i, j) >>= r_P(i, j);
    tPulledSigma(i, j) >>= r_Sigma(i, j);
    phi >>= r_phi;
    tPulledPhi_dSigma(i, j) >>= r_Flow(i, j);

    trace_off();

    MoFEMFunctionReturn(0);
  }
};

struct HMHPMooneyRivlinWriggersEq63 : public PhysicalEquations {

  static constexpr int numberOfActiveVariables = 9 + 9;
  static constexpr int numberOfDependentVariables = 9 + 9 + 1 + 9;

  HMHPMooneyRivlinWriggersEq63(const double alpha, const double beta,
                               const double lambda, const double sigma_y)
      : PhysicalEquations(numberOfActiveVariables, numberOfDependentVariables),
        alpha(alpha), beta(beta), lambda(lambda), epsilon(0), sigmaY(sigma_y) {}

  virtual OpJacobian *
  returnOpJacobian(const std::string &field_name, const int tag,
                   const bool eval_rhs, const bool eval_lhs,
                   boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                   boost::shared_ptr<PhysicalEquations> &physics_ptr) {
    return (
        new OpHMHH(field_name, tag, eval_rhs, eval_lhs, data_ptr, physics_ptr));
  }

  MoFEMErrorCode getOptions() {
    MoFEMFunctionBegin;
    CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "mooneyrivlin_", "", "none");

    alpha = 1;
    CHKERR PetscOptionsScalar("-alpha", "Alpha", "", alpha, &alpha, PETSC_NULL);
    beta = 1;
    CHKERR PetscOptionsScalar("-beta", "Beta", "", beta, &beta, PETSC_NULL);

    lambda = 1;
    CHKERR PetscOptionsScalar("-lambda", "Lambda", "", lambda, &lambda,
                              PETSC_NULL);

    epsilon = 0;
    CHKERR PetscOptionsScalar("-epsilon", "Epsilon", "", epsilon, &epsilon,
                              PETSC_NULL);

    CHKERR PetscOptionsScalar("-sigma_y", "plastic strain", "", sigmaY, &sigmaY,
                              PETSC_NULL);

    ierr = PetscOptionsEnd();
    CHKERRG(ierr);
    MoFEMFunctionReturn(0);
  }

  double alpha;
  double beta;
  double lambda;
  double epsilon;
  double sigmaY;

  ATensor2 th;
  ATensor2 tH;
  ATensor2 tF;

  adouble detH;
  adouble detF;
  ATensor2 tInvH;
  ATensor2 tInvF;

  ATensor2 tP;
  ATensor2 tSigma;
  ATensor2 tCauchy;
  ATensor2 tDevCauchy;

  ATensor2 tCof;
  // ATensor3 tCofT1;
  // ATensor3 tCofT2;
  ATensor2 tBF;
  ATensor2 tBCof;
  adouble tBj;

  adouble energy;
  adouble meanCauchy;
  adouble s2;
  adouble f;
  adouble phi;
  adouble A;
  adouble B;

  ATensor2 tPhi_dDevCauchy;
  ATensor2 tPhi_dCauchy;
  ATensor2 tPhi_dSigma;

  ATensor2 tPulledP;
  ATensor2 tPulledSigma;
  ATensor2 tPulledPhi_dSigma;

  MoFEMErrorCode recordTape(const int tape, DTensor2Ptr *t_h_ptr) {
    MoFEMFunctionBegin;

    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;
    FTensor::Index<'k', 3> k;
    FTensor::Index<'I', 3> I;
    FTensor::Index<'J', 3> J;
    FTensor::Index<'K', 3> K;
    FTensor::Number<0> N0;
    FTensor::Number<1> N1;
    FTensor::Number<2> N2;

    CHKERR getOptions();

    auto ih = get_h();
    auto iH = get_H();
    if (t_h_ptr)
      ih(i, j) = (*t_h_ptr)(i, j);
    else {
      ih(i, j) = 0;
      for (auto ii : {0, 1, 2})
        ih(ii, ii) = 1;
    }

    iH(i, j) = 0;
    for (auto ii : {0, 1, 2})
      iH(ii, ii) = 1;

    auto r_P = get_P();
    auto r_Sigma = get_Sigma();
    double &r_phi = get_PhiRef();
    auto r_Flow = get_Flow();

    enableMinMaxUsingAbs();
    trace_on(tape);

    // Set active variables to ADOL-C
    th(i, j) <<= get_h()(i, j);
    tH(i, j) <<= get_H()(i, j);

    // Deformation gradient
    CHKERR determinantTensor3by3(tH, detH);
    CHKERR invertTensor3by3(tH, detH, tInvH);

    tF(i, I) = th(i, J) * tInvH(J, I);
    CHKERR determinantTensor3by3(tF, detF);
    CHKERR invertTensor3by3(tF, detF, tInvF);

    tCof(i, I) = detF * tInvF(I, i);

    A = tF(k, K) * tF(k, K);
    B = tCof(k, K) * tCof(k, K);

    tBF(i, I) = 4 * alpha * (A * tF(i, I));
    tBCof(i, I) = 4 * beta * (B * tCof(i, I));
    tBj = (-12 * alpha - 24 * beta) / detF +
          0.5 * (lambda / epsilon) *
              (pow(detF, epsilon - 1) - pow(detF, -epsilon - 1));

    tP(i, I) = tBF(i, I);
    tP(i, I) += (levi_civita(i, j, k) * tBCof(j, J)) *
                (levi_civita(I, J, K) * tF(k, K));
    tP(i, I) += tCof(i, I) * tBj;

    // Look at equation 137 from Bonet Gil paper.
    energy = alpha * pow(A, 2) + beta * pow(B, 2);
    energy += (-12 * alpha - 24 * beta) * log(detF) +
              (lambda / (2 * epsilon * epsilon)) *
                  (pow(detF, epsilon) + pow(detF, -epsilon));

    // Stress Eshelby
    tSigma(I, J) = -tF(i, I) * tP(i, J);
    tSigma(N0, N0) += energy;
    tSigma(N1, N1) += energy;
    tSigma(N2, N2) += energy;

    // Deviator Cauchy Stress
    meanCauchy = third * tCauchy(i, i);
    tDevCauchy(i, j) = tCauchy(i, j);
    tDevCauchy(N0, N0) -= meanCauchy;
    tDevCauchy(N1, N1) -= meanCauchy;
    tDevCauchy(N2, N2) -= meanCauchy;

    // // Plastic surface

    s2 = tDevCauchy(i, j) * tDevCauchy(i, j); // s:s
    f = sqrt(1.5 * s2);
    phi = f - sigmaY;

    // Flow
    tPhi_dDevCauchy(i, j) = tDevCauchy(i, j);
    tPhi_dDevCauchy(i, j) *= 1.5 / f;
    tPhi_dCauchy(i, j) = tPhi_dDevCauchy(i, j);
    tPhi_dCauchy(N0, N0) -= third * tPhi_dDevCauchy(i, i);
    tPhi_dCauchy(N1, N1) -= third * tPhi_dDevCauchy(i, i);
    tPhi_dCauchy(N2, N2) -= third * tPhi_dDevCauchy(i, i);
    tPhi_dSigma(I, J) = tInvF(I, i) * (tPhi_dCauchy(i, j) * tF(j, J));
    tPhi_dSigma(I, J) *= -(1 / detF);

    // Pull back
    tPulledP(i, J) = tP(i, I) * tInvH(J, I);
    tPulledP(i, J) *= detH;
    tPulledSigma(i, J) = tSigma(i, I) * tInvH(J, I);
    tPulledSigma(i, J) *= detH;
    tPulledPhi_dSigma(i, J) = tPhi_dSigma(i, I) * tInvH(J, I);
    tPulledPhi_dSigma(i, J) *= detH;

    // Set dependent variables to ADOL-C
    tPulledP(i, j) >>= r_P(i, j);
    tPulledSigma(i, j) >>= r_Sigma(i, j);
    phi >>= r_phi;
    tPulledPhi_dSigma(i, j) >>= r_Flow(i, j);

    trace_off();

    MoFEMFunctionReturn(0);
  }
};

MoFEMErrorCode EshelbianCore::addMaterial_HMHHStVenantKirchhoff(
    const int tape, const double lambda, const double mu,
    const double sigma_y) {
  MoFEMFunctionBegin;
  physicalEquations = boost::shared_ptr<HMHStVenantKirchhoff>(
      new HMHStVenantKirchhoff(lambda, mu, sigma_y));
  CHKERR physicalEquations->recordTape(tape, nullptr);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode EshelbianCore::addMaterial_HMHMooneyRivlin(
    const int tape, const double alpha, const double beta, const double lambda,
    const double sigma_y) {
  MoFEMFunctionBegin;
  physicalEquations = boost::shared_ptr<HMHPMooneyRivlinWriggersEq63>(
      new HMHPMooneyRivlinWriggersEq63(alpha, beta, lambda, sigma_y));
  CHKERR physicalEquations->recordTape(tape, nullptr);
  MoFEMFunctionReturn(0);
}

}; // namespace EshelbianPlasticity