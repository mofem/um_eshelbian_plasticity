/**
 * \file EshelbianPlasticity.hpp
 * \brief Eshelbian plasticity interface
 *
 * \brief Problem implementation for mix element for large-strain elasticity
 *
 * \todo Implementation of plasticity
 */

#ifndef __ESHELBIAN_PLASTICITY_HPP__
#define __ESHELBIAN_PLASTICITY_HPP__

namespace EshelbianPlasticity {

typedef boost::shared_ptr<MatrixDouble> MatrixPtr;
typedef boost::shared_ptr<VectorDouble> VectorPtr;

inline FTensor::Tensor3<FTensor::PackPtr<double *, 1>, 3, 3, 3>
getFTensor3FromMat(MatrixDouble &m) {
  return FTensor::Tensor3<FTensor::PackPtr<double *, 1>, 3, 3, 3>(
      &m(0, 0), &m(1, 0), &m(2, 0), &m(3, 0), &m(4, 0), &m(5, 0), &m(6, 0),
      &m(7, 0), &m(8, 0), &m(9, 0), &m(10, 0), &m(11, 0), &m(12, 0), &m(13, 0),
      &m(14, 0), &m(15, 0), &m(16, 0), &m(17, 0), &m(18, 0), &m(19, 0),
      &m(20, 0), &m(21, 0), &m(22, 0), &m(23, 0), &m(24, 0), &m(25, 0),
      &m(26, 0));
}

using EntData = DataForcesAndSourcesCore::EntData;
using UserDataOperator = ForcesAndSourcesCore::UserDataOperator;
using VolUserDataOperator = VolumeElementForcesAndSourcesCore::UserDataOperator;
using FaceUserDataOperator = FaceElementForcesAndSourcesCore::UserDataOperator;

struct EpElementBase {
  SmartPetscObj<Mat> Suu;
  SmartPetscObj<AO> aoSuu;
  SmartPetscObj<Mat> SBubble;
  SmartPetscObj<AO> aoSBubble;
  SmartPetscObj<Mat> SOmega;
  SmartPetscObj<AO> aoSOmega;
  SmartPetscObj<Mat> Sw;
  SmartPetscObj<AO> aoSw;
  EpElementBase() = default;
  virtual ~EpElementBase() = default;

  MoFEMErrorCode addStreachSchurMatrix(SmartPetscObj<Mat> &Suu,
                                       SmartPetscObj<AO> &aoSuu) {
    MoFEMFunctionBegin;
    this->Suu = Suu;
    this->aoSuu = aoSuu;
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode addBubbleSchurMatrix(SmartPetscObj<Mat> &SBubble,
                                      SmartPetscObj<AO> &aoSBubble) {
    MoFEMFunctionBegin;
    this->SBubble = SBubble;
    this->aoSBubble = aoSBubble;
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode addSpatialDispStressSchurMatrix(SmartPetscObj<Mat> &Sw,
                                                 SmartPetscObj<AO> &aoSw) {
    MoFEMFunctionBegin;
    this->Sw = Sw;
    this->aoSw = aoSw;
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode addOmegaSchurMatrix(SmartPetscObj<Mat> &SOmega,
                                     SmartPetscObj<AO> &aoSOmega) {
    MoFEMFunctionBegin;
    this->SOmega = SOmega;
    this->aoSOmega = aoSOmega;
    MoFEMFunctionReturn(0);
  }
};

template <typename E> struct EpElement : public E, EpElementBase {
  EpElement(MoFEM::Interface &m_field) : E(m_field), EpElementBase() {}
};

template <> struct EpElement<BasicMethod> : public FEMethod, EpElementBase {
  EpElement() : FEMethod(), EpElementBase() {}
};

template <> struct EpElement<FEMethod> : public FEMethod, EpElementBase {
  EpElement() : FEMethod(), EpElementBase() {}
};

struct EpFEMethod : EpElement<FEMethod> {
  EpFEMethod() : EpElement<FEMethod>() {}
  MoFEMErrorCode preProcess() {
    MoFEMFunctionBegin;
    if (Suu)
      CHKERR MatZeroEntries(Suu);
    if (SBubble)
      CHKERR MatZeroEntries(SBubble);
    if (Sw)
      CHKERR MatZeroEntries(Sw);
    if (SOmega)
      CHKERR MatZeroEntries(SOmega);
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode postProcess() {
    MoFEMFunctionBegin;
    auto assemble = [](Mat a) {
      MoFEMFunctionBeginHot;
      if (a) {
        CHKERR MatAssemblyBegin(a, MAT_FINAL_ASSEMBLY);
        CHKERR MatAssemblyEnd(a, MAT_FINAL_ASSEMBLY);
      }
      MoFEMFunctionReturnHot(0);
    };
    CHKERR assemble(Suu);
    CHKERR assemble(SBubble);
    CHKERR assemble(SOmega);
    CHKERR assemble(Sw);
    // std::string wait;
    // CHKERR MatView(SOmega, PETSC_VIEWER_DRAW_WORLD);
    // std::cin >> wait;
    // CHKERR MatView(Sw, PETSC_VIEWER_DRAW_WORLD);
    // std::cin >> wait;
    // CHKERR MatView(SBubble, PETSC_VIEWER_DRAW_WORLD);
    // std::cin >> wait;
    // CHKERR MatView(Suu, PETSC_VIEWER_DRAW_WORLD);
    // std::cin >> wait;
    // CHKERR MatView(getTSB(), PETSC_VIEWER_DRAW_WORLD);
    // std::cin >> wait;
    MoFEMFunctionReturn(0);
  }
};

struct PhysicalEquations;
struct DataAtIntegrationPts
    : public boost::enable_shared_from_this<DataAtIntegrationPts> {

  MatrixDouble approxPAtPts;
  MatrixDouble approxSigmaAtPts;
  MatrixDouble divPAtPts;
  MatrixDouble divSigmaAtPts;
  MatrixDouble wAtPts;
  MatrixDouble wDotAtPts;
  MatrixDouble wDotDotAtPts;
  MatrixDouble logStreachTensorAtPts;
  MatrixDouble streachTensorAtPts;

  MatrixDouble diffStreachTensorAtPts;
  VectorDouble detStreachTensorAtPts;
  MatrixDouble detStreachTensorAtPts_du;
  MatrixDouble logStreachDotTensorAtPts;
  MatrixDouble rotAxisAtPts;
  MatrixDouble rotAxisDotAtPts;
  MatrixDouble WAtPts;
  MatrixDouble W0AtPts;
  MatrixDouble GAtPts;
  MatrixDouble G0AtPts;

  MatrixDouble hAtPts;
  MatrixDouble rotMatAtPts;
  MatrixDouble diffRotMatAtPts;
  MatrixDouble PAtPts;
  MatrixDouble SigmaAtPts;
  VectorDouble phiAtPts;
  MatrixDouble flowAtPts;

  MatrixDouble P_dh0;
  MatrixDouble P_dh1;
  MatrixDouble P_dh2;
  MatrixDouble P_dH0;
  MatrixDouble P_dH1;
  MatrixDouble P_dH2;
  MatrixDouble Sigma_dh0;
  MatrixDouble Sigma_dh1;
  MatrixDouble Sigma_dh2;
  MatrixDouble Sigma_dH0;
  MatrixDouble Sigma_dH1;
  MatrixDouble Sigma_dH2;
  MatrixDouble phi_dh;
  MatrixDouble phi_dH;
  MatrixDouble Flow_dh0;
  MatrixDouble Flow_dh1;
  MatrixDouble Flow_dh2;
  MatrixDouble Flow_dH0;
  MatrixDouble Flow_dH1;
  MatrixDouble Flow_dH2;

  MatrixDouble eigenVals;
  MatrixDouble eigenVecs;
  VectorInt nbUniq;

  inline MatrixPtr getApproxSigmaAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(),
                                           &approxSigmaAtPts);
  }
  inline MatrixPtr getApproxPAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &approxPAtPts);
  }

  inline MatrixPtr getDivPAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &divPAtPts);
  }

  inline MatrixPtr getDivSigmaAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &divSigmaAtPts);
  }

  inline MatrixPtr getSmallWAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &wAtPts);
  }

  inline MatrixPtr getSmallWDotAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &wDotAtPts);
  }

  inline MatrixPtr getSmallWDotDotAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &wDotDotAtPts);
  }

  inline MatrixPtr getLogStreachTensorAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(),
                                           &logStreachTensorAtPts);
  }

  inline MatrixPtr getStreachTensorAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(),
                                           &streachTensorAtPts);
  }

  inline MatrixPtr getLogStreachDotTensorAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(),
                                           &logStreachDotTensorAtPts);
  }

  inline MatrixPtr getRotAxisAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &rotAxisAtPts);
  }

  inline MatrixPtr getRotAxisDotAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(),
                                           &rotAxisDotAtPts);
  }

  inline MatrixPtr getBigGAtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &GAtPts);
  }

  inline MatrixPtr getBigG0AtPts() {
    return boost::shared_ptr<MatrixDouble>(shared_from_this(), &GAtPts);
  }

  // Not really data at integration points, used to calculate Schur complement

  struct BlockMatData {

    std::string rowField;
    std::string colField;
    EntityType rowType;
    EntityType colType;
    int rowSide;
    int colSide;
    MatrixDouble M;
    VectorInt rowInd;
    VectorInt colInd;

    bool setAtElement;

    BlockMatData(const std::string row_field, const std::string col_field,
                 EntityType row_type, EntityType col_type, int row_side,
                 int col_side, const MatrixDouble &m, const VectorInt row_ind,
                 VectorInt col_ind)
        : rowField(row_field), colField(col_field), rowType(row_type),
          colType(col_type), rowSide(row_side), colSide(col_side),
          setAtElement(true) {

      M.resize(m.size1(), m.size2(), false);
      noalias(M) = m;
      rowInd.resize(row_ind.size(), false);
      noalias(rowInd) = row_ind;
      colInd.resize(col_ind.size(), false);
      noalias(colInd) = col_ind;
    }

    void setInd(const VectorInt &row_ind, const VectorInt &col_ind) const {
      auto &const_row_ind = const_cast<VectorInt &>(rowInd);
      auto &const_col_ind = const_cast<VectorInt &>(colInd);
      const_row_ind.resize(row_ind.size(), false);
      noalias(const_row_ind) = row_ind;
      const_col_ind.resize(col_ind.size(), false);
      noalias(const_col_ind) = col_ind;
    }

    void setMat(const MatrixDouble &m) const {
      auto &const_m = const_cast<MatrixDouble &>(M);
      const_m.resize(m.size1(), m.size2(), false);
      noalias(const_m) = m;
    }

    void addMat(const MatrixDouble &m) const {
      auto &const_m = const_cast<MatrixDouble &>(M);
      const_m += m;
    }

    void clearMat() const {
      auto &const_m = const_cast<MatrixDouble &>(M);
      const_m.clear();
    }

    void setSetAtElement() const {
      bool &set = const_cast<bool &>(setAtElement);
      set = true;
    }

    void unSetAtElement() const {
      bool &set = const_cast<bool &>(setAtElement);
      set = false;
    }
  };

  typedef multi_index_container<
      BlockMatData,
      indexed_by<

          ordered_unique<

              composite_key<
                  BlockMatData,

                  member<BlockMatData, std::string, &BlockMatData::rowField>,
                  member<BlockMatData, std::string, &BlockMatData::colField>,
                  member<BlockMatData, EntityType, &BlockMatData::rowType>,
                  member<BlockMatData, EntityType, &BlockMatData::colType>,
                  member<BlockMatData, int, &BlockMatData::rowSide>,
                  member<BlockMatData, int, &BlockMatData::colSide>

                  >>,
          ordered_non_unique<

              composite_key<
                  BlockMatData,

                  member<BlockMatData, std::string, &BlockMatData::rowField>,
                  member<BlockMatData, std::string, &BlockMatData::colField>,
                  member<BlockMatData, EntityType, &BlockMatData::rowType>,
                  member<BlockMatData, EntityType, &BlockMatData::colType>

                  >>,
          ordered_non_unique<

              composite_key<
                  BlockMatData,

                  member<BlockMatData, std::string, &BlockMatData::rowField>,
                  member<BlockMatData, std::string, &BlockMatData::colField>

                  >>,
          ordered_non_unique<
              member<BlockMatData, std::string, &BlockMatData::rowField>>,
          ordered_non_unique<
              member<BlockMatData, std::string, &BlockMatData::colField>>

          >>
      BlockMatContainor;

  BlockMatContainor blockMatContainor;
  MatrixPtr wwMatPtr;
  MatrixPtr ooMatPtr;

  boost::shared_ptr<PhysicalEquations> physicsPtr;
};

struct OpJacobian;

struct PhysicalEquations {

  typedef FTensor::Tensor1<adouble, 3> ATensor1;
  typedef FTensor::Tensor2<adouble, 3, 3> ATensor2;
  typedef FTensor::Tensor3<adouble, 3, 3, 3> ATensor3;
  typedef FTensor::Tensor0<FTensor::PackPtr<double *, 1>> DTensor0Ptr;

  typedef FTensor::Tensor2<FTensor::PackPtr<double *, 1>, 3, 3> DTensor2Ptr;
  typedef FTensor::Tensor3<FTensor::PackPtr<double *, 1>, 3, 3, 3> DTensor3Ptr;

  PhysicalEquations() = delete;
  PhysicalEquations(const int size_active, const int size_dependent)
      : activeVariables(size_active, 0), dependentVariables(size_dependent, 0),
        dependentVariablesDirevatives(size_dependent * size_active, 0) {}
  virtual ~PhysicalEquations() {}

  virtual MoFEMErrorCode recordTape(const int tag, DTensor2Ptr *t_h) = 0;

  virtual OpJacobian *
  returnOpJacobian(const std::string &field_name, const int tag,
                   const bool eval_rhs, const bool eval_lhs,
                   boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                   boost::shared_ptr<PhysicalEquations> &physics_ptr) = 0;

  std::vector<double> activeVariables;
  std::vector<double> dependentVariables;
  std::vector<double> dependentVariablesDirevatives;

  /** \name Active variables */

  /**@{*/

  template <int S>
  inline static DTensor2Ptr get_VecTensor2(std::vector<double> &v) {
    return DTensor2Ptr(&v[S + 0], &v[S + 1], &v[S + 2], &v[S + 3], &v[S + 4],
                       &v[S + 5], &v[S + 6], &v[S + 7], &v[S + 8]);
  }

  template <int S>
  inline static DTensor0Ptr get_VecTensor0(std::vector<double> &v) {
    return DTensor0Ptr(&v[S + 0]);
  }

  template <int S0, int S1, int S2>
  inline static DTensor3Ptr get_vecTensor3(std::vector<double> &v) {
    constexpr int A00 = 18 * S0 + 18 * 0 + 9 * S1 + 3 * S2;
    constexpr int A01 = 18 * S0 + 18 * 1 + 9 * S1 + 3 * S2;
    constexpr int A02 = 18 * S0 + 18 * 2 + 9 * S1 + 3 * S2;
    constexpr int A10 = 18 * S0 + 18 * 3 + 9 * S1 + 3 * S2;
    constexpr int A11 = 18 * S0 + 18 * 4 + 9 * S1 + 3 * S2;
    constexpr int A12 = 18 * S0 + 18 * 5 + 9 * S1 + 3 * S2;
    constexpr int A20 = 18 * S0 + 18 * 6 + 9 * S1 + 3 * S2;
    constexpr int A21 = 18 * S0 + 18 * 7 + 9 * S1 + 3 * S2;
    constexpr int A22 = 18 * S0 + 18 * 8 + 9 * S1 + 3 * S2;
    return DTensor3Ptr(

        &v[A00 + 0], &v[A00 + 1], &v[A00 + 2], &v[A01 + 0], &v[A01 + 1],
        &v[A01 + 2], &v[A02 + 0], &v[A02 + 1], &v[A02 + 2],

        &v[A10 + 0], &v[A10 + 1], &v[A10 + 2], &v[A11 + 0], &v[A11 + 1],
        &v[A11 + 2], &v[A12 + 0], &v[A12 + 1], &v[A12 + 2],

        &v[A20 + 0], &v[A20 + 1], &v[A20 + 2], &v[A21 + 0], &v[A21 + 1],
        &v[A21 + 2], &v[A22 + 0], &v[A22 + 1], &v[A22 + 2]

    );
  }

  /**@}*/

  /** \name Active variables */

  /**@{*/

  inline DTensor2Ptr get_h() { return get_VecTensor2<0>(activeVariables); }
  inline DTensor2Ptr get_H() { return get_VecTensor2<9>(activeVariables); }

  /**@}*/

  /** \name Dependent variables */

  /**@{*/

  inline DTensor2Ptr get_P() { return get_VecTensor2<0>(dependentVariables); }
  inline DTensor2Ptr get_Sigma() {
    return get_VecTensor2<9>(dependentVariables);
  }
  inline DTensor0Ptr get_Phi() {
    return get_VecTensor0<18>(dependentVariables);
  }
  inline double &get_PhiRef() { return dependentVariables[18]; }
  inline DTensor2Ptr get_Flow() {
    return get_VecTensor2<9 + 9 + 1>(dependentVariables);
  }

  /**@}*/

  /** \name Derivatives of dependent variables */

  /**@{*/

  inline DTensor3Ptr get_P_dh0() {
    return get_vecTensor3<0, 0, 0>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_P_dH0() {
    return get_vecTensor3<0, 1, 0>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_P_dh1() {
    return get_vecTensor3<0, 0, 1>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_P_dH1() {
    return get_vecTensor3<0, 1, 1>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_P_dh2() {
    return get_vecTensor3<0, 0, 2>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_P_dH2() {
    return get_vecTensor3<0, 1, 2>(dependentVariablesDirevatives);
  }

  inline DTensor3Ptr get_Sigma_dh0() {
    return get_vecTensor3<9, 0, 0>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Sigma_dH0() {
    return get_vecTensor3<9, 1, 0>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Sigma_dh1() {
    return get_vecTensor3<9, 0, 1>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Sigma_dH1() {
    return get_vecTensor3<9, 1, 1>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Sigma_dh2() {
    return get_vecTensor3<9, 0, 2>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Sigma_dH2() {
    return get_vecTensor3<9, 1, 2>(dependentVariablesDirevatives);
  }

  inline DTensor2Ptr get_Phi_dh() {
    return get_VecTensor2<18 * (9 + 9) + 0>(dependentVariablesDirevatives);
  }
  inline DTensor2Ptr get_Phi_dH() {
    return get_VecTensor2<18 * (9 + 9) + 9>(dependentVariablesDirevatives);
  }

  inline DTensor3Ptr get_Flow_dh0() {
    return get_vecTensor3<9 + 9 + 1, 0, 0>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Flow_dH0() {
    return get_vecTensor3<9 + 9 + 1, 1, 0>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Flow_dh1() {
    return get_vecTensor3<9 + 9 + 1, 0, 1>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Flow_dH1() {
    return get_vecTensor3<9 + 9 + 1, 1, 1>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Flow_dh2() {
    return get_vecTensor3<9 + 9 + 1, 0, 2>(dependentVariablesDirevatives);
  }
  inline DTensor3Ptr get_Flow_dH2() {
    return get_vecTensor3<9 + 9 + 1, 1, 2>(dependentVariablesDirevatives);
  }

  /**@}*/
};

struct BcDisp {
  BcDisp(std::string name, std::vector<double> &attr, Range &faces);
  std::string blockName;
  Range faces;
  VectorDouble3 vals;
  VectorInt3 flags;
};
typedef std::vector<BcDisp> BcDispVec;

struct BcRot {
  BcRot(std::string name, std::vector<double> &attr, Range &faces);
  std::string blockName;
  Range faces;
  VectorDouble3 vals;
  double theta;
};
typedef std::vector<BcRot> BcRotVec;

typedef std::vector<Range> TractionFreeBc;

struct TractionBc {
  TractionBc(std::string name, std::vector<double> &attr, Range &faces);
  std::string blockName;
  Range faces;
  VectorDouble3 vals;
  VectorInt3 flags;
};
typedef std::vector<TractionBc> TractionBcVec;

struct OpJacobian : public UserDataOperator {
  const int tAg; ///< adol-c tape
  const bool evalRhs;
  const bool evalLhs;
  boost::shared_ptr<DataAtIntegrationPts>
      dataAtPts; ///< data at integration pts
  boost::shared_ptr<PhysicalEquations>
      physicsPtr; ///< material physical equations

  OpJacobian(const std::string &field_name, const int tag, const bool eval_rhs,
             const bool eval_lhs,
             boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
             boost::shared_ptr<PhysicalEquations> &physics_ptr)
      : UserDataOperator(field_name, OPROW), tAg(tag), evalRhs(eval_rhs),
        evalLhs(eval_lhs), dataAtPts(data_ptr), physicsPtr(physics_ptr) {}

  virtual MoFEMErrorCode evaluateRhs(EntData &data) = 0;
  virtual MoFEMErrorCode evaluateLhs(EntData &data) = 0;

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data);
};

template <typename T> struct OpAssembleBasic : public T {

  const bool assembleSymmetry;

  boost::shared_ptr<DataAtIntegrationPts>
      dataAtPts; ///< data at integration pts

  OpAssembleBasic(const std::string &field_name,
                  boost::shared_ptr<DataAtIntegrationPts> data_ptr,
                  const char type)
      : T(field_name, type), dataAtPts(data_ptr), assembleSymmetry(false) {}

  OpAssembleBasic(const std::string &row_field, const std::string &col_field,
                  boost::shared_ptr<DataAtIntegrationPts> data_ptr,
                  const char type, const bool assemble_symmetry)
      : T(row_field, col_field, type, false), dataAtPts(data_ptr),
        assembleSymmetry(assemble_symmetry) {}

  VectorDouble nF; ///< local right hand side vector
  MatrixDouble K;  ///< local tangent matrix
  MatrixDouble transposeK;

  virtual MoFEMErrorCode integrate(EntData &data) {
    MoFEMFunctionBegin;
    SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_IMPLEMENTED, "Not yet implemented");
    MoFEMFunctionReturn(0);
  }

  virtual MoFEMErrorCode integrate(int row_side, EntityType row_type,
                                   EntData &data) {
    MoFEMFunctionBegin;
    CHKERR integrate(data);
    MoFEMFunctionReturn(0);
  }

  virtual MoFEMErrorCode assemble(EntData &data) {
    MoFEMFunctionBegin;
    double *vec_ptr = &*nF.begin();
    int nb_dofs = data.getIndices().size();
    int *ind_ptr = &*data.getIndices().begin();
    CHKERR VecSetValues(this->getTSf(), nb_dofs, ind_ptr, vec_ptr, ADD_VALUES);
    MoFEMFunctionReturn(0);
  }

  virtual MoFEMErrorCode assemble(int row_side, EntityType row_type,
                                  EntData &data) {
    MoFEMFunctionBegin;
    CHKERR assemble(data);
    MoFEMFunctionReturn(0);
  }

  virtual MoFEMErrorCode integrate(EntData &row_data, EntData &col_data) {
    MoFEMFunctionBegin;
    SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_IMPLEMENTED, "Not yet implemented");
    MoFEMFunctionReturn(0);
  }

  virtual MoFEMErrorCode assemble(int row_side, int col_side,
                                  EntityType row_type, EntityType col_type,
                                  EntData &row_data, EntData &col_data) {
    MoFEMFunctionBegin;

    auto &bmc = dataAtPts->blockMatContainor;
    int *row_ind_ptr = &*row_data.getIndices().begin();
    int *col_ind_ptr = &*col_data.getIndices().begin();
    int row_nb_dofs = row_data.getIndices().size();
    int col_nb_dofs = col_data.getIndices().size();

    auto add_block = [&](auto &row_name, auto &col_name, auto row_side,
                         auto col_side, auto row_type, auto col_type,
                         const auto &m, const auto &row_ind,
                         const auto &col_ind) {
      auto it = bmc.get<0>().find(boost::make_tuple(
          row_name, col_name, row_type, col_type, row_side, col_side));
      if (it != bmc.get<0>().end()) {
        it->setMat(m);
        it->setInd(row_ind, col_ind);
        it->setSetAtElement();
      } else
        bmc.insert(DataAtIntegrationPts::BlockMatData(
            row_name, col_name, row_type, col_type, row_side, col_side, m,
            row_ind, col_ind));
    };

    add_block(this->rowFieldName, this->colFieldName, row_side, col_side,
              row_type, col_type, K, row_data.getIndices(),
              col_data.getIndices());
    if (assembleSymmetry) {
      transposeK.resize(col_nb_dofs, row_nb_dofs, false);
      noalias(transposeK) = trans(K);
      add_block(this->colFieldName, this->rowFieldName, col_side, row_side,
                col_type, row_type, transposeK, col_data.getIndices(),
                row_data.getIndices());
    }

    double *mat_ptr = &*K.data().begin();
    CHKERR MatSetValues(this->getTSB(), row_nb_dofs, row_ind_ptr, col_nb_dofs,
                        col_ind_ptr, mat_ptr, ADD_VALUES);
    if (assembleSymmetry) {
      double *mat_ptr = &*transposeK.data().begin();
      CHKERR MatSetValues(this->getTSB(), col_nb_dofs, col_ind_ptr, row_nb_dofs,
                          row_ind_ptr, mat_ptr, ADD_VALUES);
    }

    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    if (data.getIndices().empty())
      MoFEMFunctionReturnHot(0);
    nF.resize(data.getIndices().size(), false);
    nF.clear();
    CHKERR integrate(side, type, data);
    CHKERR assemble(side, type, data);
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    if (row_data.getIndices().empty())
      MoFEMFunctionReturnHot(0);
    if (col_data.getIndices().empty())
      MoFEMFunctionReturnHot(0);
    K.resize(row_data.getIndices().size(), col_data.getIndices().size(), false);
    K.clear();
    CHKERR integrate(row_data, col_data);
    CHKERR assemble(row_side, col_side, row_type, col_type, row_data, col_data);
    MoFEMFunctionReturn(0);
  }
};

struct OpAssembleVolume : public OpAssembleBasic<VolUserDataOperator> {
  OpAssembleVolume(const std::string &field,
                   boost::shared_ptr<DataAtIntegrationPts> data_ptr,
                   const char type)
      : OpAssembleBasic<VolUserDataOperator>(field, data_ptr, type) {}

  OpAssembleVolume(const std::string &row_field, const std::string &col_field,
                   boost::shared_ptr<DataAtIntegrationPts> data_ptr,
                   const char type, const bool assemble_symmetry)
      : OpAssembleBasic<VolUserDataOperator>(row_field, col_field, data_ptr,
                                             type, assemble_symmetry) {}
};

struct OpAssembleFace : public OpAssembleBasic<FaceUserDataOperator> {
  OpAssembleFace(const std::string &field,
                 boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                 const char type)
      : OpAssembleBasic<FaceUserDataOperator>(field, data_ptr, type) {}

  OpAssembleFace(const std::string &row_field, const std::string &col_field,
                 boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                 const char type, const bool assemble_symmetry)
      : OpAssembleBasic<FaceUserDataOperator>(row_field, col_field, data_ptr,
                                              type, assemble_symmetry) {}
};

struct OpCalculateRotationAndSpatialGradient : public VolUserDataOperator {
  boost::shared_ptr<DataAtIntegrationPts>
      dataAtPts; ///< data at integration pts
  OpCalculateRotationAndSpatialGradient(
      const std::string &field_name,
      boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : VolUserDataOperator(field_name, OPROW), dataAtPts(data_ptr) {}
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data);
};

struct OpSpatialEquilibrium : public OpAssembleVolume {
  const double alphaW;
  const double alphaRho;
  OpSpatialEquilibrium(const std::string &field_name,
                       boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                       const double alpha, const double rho)
      : OpAssembleVolume(field_name, data_ptr, OPROW), alphaW(alpha),
        alphaRho(rho) {}
  MoFEMErrorCode integrate(EntData &data);
};

struct OpSpatialRotation : public OpAssembleVolume {
  OpSpatialRotation(const std::string &field_name,
                    boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(field_name, data_ptr, OPROW) {}
  MoFEMErrorCode integrate(EntData &data);
};

struct OpSpatialPhysical : public OpAssembleVolume {
  const double alphaU;
  OpSpatialPhysical(const std::string &field_name,
                    boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                    const double alpha)
      : OpAssembleVolume(field_name, data_ptr, OPROW), alphaU(alpha) {}
  MoFEMErrorCode integrate(EntData &data);
};

struct OpSpatialConsistencyP : public OpAssembleVolume {
  OpSpatialConsistencyP(const std::string &field_name,
                        boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(field_name, data_ptr, OPROW) {}
  MoFEMErrorCode integrate(EntData &data);
};

struct OpSpatialConsistencyBubble : public OpAssembleVolume {
  OpSpatialConsistencyBubble(const std::string &field_name,
                             boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(field_name, data_ptr, OPROW) {}
  MoFEMErrorCode integrate(EntData &data);
};

struct OpSpatialConsistencyDivTerm : public OpAssembleVolume {
  OpSpatialConsistencyDivTerm(const std::string &field_name,
                              boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(field_name, data_ptr, OPROW) {}
  MoFEMErrorCode integrate(EntData &data);
};

struct OpDispBc : public OpAssembleFace {
  boost::shared_ptr<BcDispVec> bcDispPtr;
  OpDispBc(const std::string &field_name,
           boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
           boost::shared_ptr<BcDispVec> &bc_disp_ptr)
      : OpAssembleFace(field_name, data_ptr, OPROW), bcDispPtr(bc_disp_ptr) {}
  MoFEMErrorCode integrate(EntData &data);
};

struct OpDispBc_dx : public OpAssembleFace {
  boost::shared_ptr<BcDispVec> bcDispPtr;
  OpDispBc_dx(const std::string &row_field_name,
              const std::string &col_field_name,
              boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
              boost::shared_ptr<BcDispVec> &bc_disp_ptr)
      : OpAssembleFace(row_field_name, col_field_name, data_ptr, OPROWCOL,
                       false),
        bcDispPtr(bc_disp_ptr) {}
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpRotationBc : public OpAssembleFace {
  boost::shared_ptr<BcRotVec> bcRotPtr;
  OpRotationBc(const std::string &field_name,
               boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
               boost::shared_ptr<BcRotVec> &bc_rot_ptr)
      : OpAssembleFace(field_name, data_ptr, OPROW), bcRotPtr(bc_rot_ptr) {}
  MoFEMErrorCode integrate(EntData &data);
};

struct OpRotationBc_dx : public OpAssembleFace {
  boost::shared_ptr<BcRotVec> bcRotPtr;
  OpRotationBc_dx(const std::string &row_field_name,
                  const std::string &col_field_name,
                  boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                  boost::shared_ptr<BcRotVec> &bc_rot_ptr)
      : OpAssembleFace(row_field_name, col_field_name, data_ptr, OPROWCOL,
                       false),
        bcRotPtr(bc_rot_ptr) {}
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct FeTractionBc;

struct OpTractionBc : public FaceUserDataOperator {
  OpTractionBc(std::string row_field, FeTractionBc &bc_fe)
      : FaceUserDataOperator(row_field, FaceUserDataOperator::OPROW),
        bcFe(bc_fe) {}
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

protected:
  FeTractionBc &bcFe;
  MatrixDouble matPP;
  MatrixDouble vecPv;
};

struct FeTractionBc : public FEMethod {
  int nbConstrainsDofs;
  FeTractionBc(MoFEM::Interface &m_field, const std::string field_name,
               boost::shared_ptr<TractionBcVec> &bc)
      : FEMethod(), mField(m_field), bcData(bc), fieldName(field_name) {}
  MoFEMErrorCode preProcess();
  MoFEMErrorCode postProcess() { return 0; }

  friend MoFEMErrorCode OpTractionBc::doWork(int side, EntityType type,
                                             EntData &data);

protected:
  MoFEM::Interface &mField;
  boost::shared_ptr<TractionBcVec> bcData;
  std::string fieldName;
};

template <>
struct EpElement<FeTractionBc> : public FeTractionBc, EpElementBase {
  EpElement(MoFEM::Interface &m_field, const std::string field_name,
            boost::shared_ptr<TractionBcVec> &bc)
      : FeTractionBc(m_field, field_name, bc), EpElementBase() {}
  MoFEMErrorCode postProcess() {
    MoFEMFunctionBegin;
    CHKERR FeTractionBc::postProcess();
    MoFEMFunctionReturn(0);
  }
};

struct OpSpatialEquilibrium_dw_dP : public OpAssembleVolume {
  OpSpatialEquilibrium_dw_dP(const std::string &row_field,
                             const std::string &col_field,
                             boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                             const bool assemble_off = false)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL,
                         assemble_off) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialEquilibrium_dw_dw : public OpAssembleVolume {
  const double alphaW;
  const double alphaRho;
  OpSpatialEquilibrium_dw_dw(const std::string &row_field,
                             const std::string &col_field,
                             boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                             const double alpha, const double rho)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL, false),
        alphaW(alpha), alphaRho(rho) {
    sYmm = true;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialPhysical_du_du : public OpAssembleVolume {
  const double alphaU;
  OpSpatialPhysical_du_du(const std::string &row_field,
                          const std::string &col_field,
                          boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                          const double alpha)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL, false),
        alphaU(alpha) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialPhysical_du_dP : public OpAssembleVolume {
  OpSpatialPhysical_du_dP(const std::string &row_field,
                          const std::string &col_field,
                          boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                          const bool assemble_off = false)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL,
                         assemble_off) {
    sYmm = false;
  }

  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialPhysical_du_dBubble : public OpAssembleVolume {
  OpSpatialPhysical_du_dBubble(
      const std::string &row_field, const std::string &col_field,
      boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
      const bool assemble_off = false)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL,
                         assemble_off) {
    sYmm = false;
  }

  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialPhysical_du_domega : public OpAssembleVolume {
  OpSpatialPhysical_du_domega(const std::string &row_field,
                              const std::string &col_field,
                              boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                              const bool assemble_off = false)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL,
                         assemble_off) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialPhysical_du_dx : public OpAssembleVolume {
  OpSpatialPhysical_du_dx(const std::string &row_field,
                          const std::string &col_field,
                          boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                          const bool assemble_off = false)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL,
                         assemble_off) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialRotation_domega_dP : public OpAssembleVolume {
  OpSpatialRotation_domega_dP(const std::string &row_field,
                              const std::string &col_field,
                              boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                              const bool assemble_off)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL,
                         assemble_off) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialRotation_domega_dBubble : public OpAssembleVolume {
  OpSpatialRotation_domega_dBubble(
      const std::string &row_field, const std::string &col_field,
      boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
      const bool assemble_off)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL,
                         assemble_off) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialRotation_domega_domega : public OpAssembleVolume {
  OpSpatialRotation_domega_domega(
      const std::string &row_field, const std::string &col_field,
      boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
      const bool assemble_off = false)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL,
                         assemble_off) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialConsistency_dP_domega : public OpAssembleVolume {
  OpSpatialConsistency_dP_domega(
      const std::string &row_field, const std::string &col_field,
      boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL, false) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialConsistency_dBubble_domega : public OpAssembleVolume {
  OpSpatialConsistency_dBubble_domega(
      const std::string &row_field, const std::string &col_field,
      boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL, false) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpPostProcDataStructure : public VolUserDataOperator {

  moab::Interface &postProcMesh;
  std::vector<EntityHandle> &mapGaussPts;
  boost::shared_ptr<DataAtIntegrationPts> dataAtPts;

  OpPostProcDataStructure(moab::Interface &post_proc_mesh,
                          std::vector<EntityHandle> &map_gauss_pts,
                          const std::string field_name,
                          boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : VolUserDataOperator(field_name, UserDataOperator::OPROW),
        postProcMesh(post_proc_mesh), mapGaussPts(map_gauss_pts),
        dataAtPts(data_ptr) {}

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data);
};

struct OpSpatialPrj : public OpAssembleVolume {
  OpSpatialPrj(const std::string &row_field,
               boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(row_field, data_ptr, OPROW) {}
  MoFEMErrorCode integrate(EntData &row_data);
};

struct OpSpatialPrj_dx_dx : public OpAssembleVolume {
  OpSpatialPrj_dx_dx(const std::string &row_field, const std::string &col_field,
                     boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL, false) {
    // FIXME: That is symmetric
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialPrj_dx_dw : public OpAssembleVolume {
  OpSpatialPrj_dx_dw(const std::string &row_field, const std::string &col_field,
                     boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(row_field, col_field, data_ptr, OPROWCOL, false) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data, EntData &col_data);
};

struct OpSpatialSchurBegin : public OpAssembleVolume {
  OpSpatialSchurBegin(const std::string &row_field,
                      boost::shared_ptr<DataAtIntegrationPts> &data_ptr)
      : OpAssembleVolume(row_field, data_ptr, OPROW) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data) { return 0; }
  MoFEMErrorCode assemble(int row_side, EntityType row_type, EntData &data);
};

struct OpSpatialPreconditionMass : public OpAssembleVolume {
  OpSpatialPreconditionMass(const std::string &row_field, MatrixPtr m_ptr)
      : OpAssembleVolume(row_field, nullptr, OPROW), mPtr(m_ptr) {
    sYmm = false;
  }
  MoFEMErrorCode integrate(EntData &row_data);
  MoFEMErrorCode assemble(int row_side, EntityType row_type, EntData &data) {
    return 0;
  }

private:
  MatrixPtr mPtr;
};

struct OpSpatialSchurEnd : public OpAssembleVolume {
  OpSpatialSchurEnd(const std::string &row_field,
                    boost::shared_ptr<DataAtIntegrationPts> &data_ptr,
                    const double eps)
      : OpAssembleVolume(row_field, data_ptr, OPROW), eps(eps) {
    sYmm = false;
  }

  MoFEMErrorCode integrate(EntData &row_data) { return 0; }
  MoFEMErrorCode assemble(int side, EntityType type, EntData &data);
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    return assemble(side, type, data);
  }

private:
  const double eps;

  MatrixDouble invMat;
  VectorInt iPIV;
  VectorDouble lapackWork;

  map<std::string, MatrixDouble> invBlockMat;
  map<std::string, DataAtIntegrationPts::BlockMatContainor> blockMat;
};

struct OpCalculateStrainEnergy : public VolUserDataOperator {

  OpCalculateStrainEnergy(const std::string field_name,
                          boost::shared_ptr<DataAtIntegrationPts> data_at_pts,
                          boost::shared_ptr<double> &e)
      : VolUserDataOperator(field_name, VolUserDataOperator::OPROW),
        dataAtPts(data_at_pts), energy(e) {}

  MoFEMErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data);

private:
  boost::shared_ptr<DataAtIntegrationPts> dataAtPts;
  boost::shared_ptr<double> energy;
};

struct OpL2Transform : public VolUserDataOperator {

  OpL2Transform() : VolUserDataOperator(L2) {}

  MoFEMErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data);
};

struct EshelbianCore : public MoFEM::UnknownInterface {

  /**
   * \brief Getting interface of core database
   * @param  uuid  unique ID of interface
   * @param  iface returned pointer to interface
   * @return       error code
   */
  MoFEMErrorCode query_interface(boost::typeindex::type_index type_index,
                                 UnknownInterface **iface) const;

  MoFEM::Interface &mField;

  boost::shared_ptr<DataAtIntegrationPts> dataAtPts;
  boost::shared_ptr<PhysicalEquations> physicalEquations;

  boost::shared_ptr<EpElement<VolumeElementForcesAndSourcesCore>> elasticFeRhs;
  boost::shared_ptr<EpElement<VolumeElementForcesAndSourcesCore>> elasticFeLhs;
  boost::shared_ptr<EpElement<FaceElementForcesAndSourcesCore>> elasticBcLhs;
  boost::shared_ptr<EpElement<FaceElementForcesAndSourcesCore>> elasticBcRhs;
  boost::shared_ptr<EpFEMethod> schurAssembly;

  SmartPetscObj<DM> dM;                    ///< Coupled problem all fields
  SmartPetscObj<DM> dmElastic;             ///< Elastic problem
  SmartPetscObj<DM> dmElasticSchurStreach; ///< Sub problem of dmElastic Schur
  SmartPetscObj<DM> dmElasticSchurBubble;  ///< Sub problem of dmElastic Schur
  SmartPetscObj<DM>
      dmElasticSchurSpatialDisp;         ///< Sub problem of dmElastic Schur
  SmartPetscObj<DM> dmElasticSchurOmega; ///< Sub problem of dmElastic Schur
  SmartPetscObj<DM> dmMaterial;          ///< Material problem

  const std::string piolaStress;
  const std::string eshelbyStress;
  const std::string spatialDisp;
  const std::string materialDisp;
  const std::string streachTensor;
  const std::string rotAxis;
  const std::string materialGradient;
  const std::string tauField;
  const std::string lambdaField;
  const std::string bubbleField;

  const std::string elementVolumeName;
  const std::string naturalBcElement;
  const std::string essentialBcElement;

  EshelbianCore(MoFEM::Interface &m_field);
  virtual ~EshelbianCore();

  int spaceOrder;
  int materialOrder;
  double alphaU;
  double alphaW;
  double alphaRho;
  double precEps;

  MoFEMErrorCode getOptions();

  boost::shared_ptr<BcDispVec> bcSpatialDispVecPtr;
  boost::shared_ptr<BcRotVec> bcSpatialRotationVecPtr;
  boost::shared_ptr<TractionBcVec> bcSpatialTraction;
  boost::shared_ptr<TractionFreeBc> bcSpatialFreeTraction;

  template <typename BC>
  MoFEMErrorCode getBc(boost::shared_ptr<BC> &bc_vec_ptr,
                       const std::string block_set_name,
                       const int nb_attributes) {
    MoFEMFunctionBegin;
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
      auto block_name = it->getName();
      if (block_name.compare(0, block_set_name.length(), block_set_name) == 0) {
        std::vector<double> block_attributes;
        CHKERR it->getAttributes(block_attributes);
        if (block_attributes.size() != nb_attributes) {
          SETERRQ3(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                   "In block %s expected %d attributes, but given %d",
                   it->getName().c_str(), nb_attributes,
                   block_attributes.size());
        }
        Range faces;
        CHKERR it->getMeshsetIdEntitiesByDimension(mField.get_moab(), 2, faces,
                                                   true);
        bc_vec_ptr->emplace_back(block_name, block_attributes, faces);
      }
    }
    MoFEMFunctionReturn(0);
  }

  inline MoFEMErrorCode getSpatialDispBc() {
    bcSpatialDispVecPtr = boost::make_shared<BcDispVec>();
    return getBc(bcSpatialDispVecPtr, "SPATIAL_DISP_BC", 6);
  }

  inline MoFEMErrorCode getSpatialRotationBc() {
    bcSpatialRotationVecPtr = boost::make_shared<BcRotVec>();
    return getBc(bcSpatialRotationVecPtr, "SPATIAL_ROTATION_BC", 4);
  }

  inline MoFEMErrorCode getSpatialTractionBc() {
    bcSpatialTraction = boost::make_shared<TractionBcVec>();
    return getBc(bcSpatialTraction, "SPATIAL_TRACTION_BC", 6);
  }

  MoFEMErrorCode getTractionFreeBc(const EntityHandle meshset,
                                   boost::shared_ptr<TractionFreeBc> &bc_ptr,
                                   const std::string disp_block_set_name,
                                   const std::string rot_block_set_name);
  inline MoFEMErrorCode
  getSpatialTractionFreeBc(const EntityHandle meshset = 0) {
    bcSpatialFreeTraction =
        boost::shared_ptr<TractionFreeBc>(new TractionFreeBc());
    return getTractionFreeBc(meshset, bcSpatialFreeTraction, "SPATIAL_DISP_BC",
                             "SPATIAL_ROTATION_BC");
  }

  MoFEMErrorCode addFields(const EntityHandle meshset = 0);
  MoFEMErrorCode addVolumeFiniteElement(const EntityHandle meshset = 0);
  MoFEMErrorCode addBoundaryFiniteElement(const EntityHandle meshset = 0);
  MoFEMErrorCode addDMs(const BitRefLevel bit = BitRefLevel().set(0));

  MoFEMErrorCode addMaterial_HMHHStVenantKirchhoff(const int tape,
                                                   const double lambda,
                                                   const double mu,
                                                   const double sigma_y);

  MoFEMErrorCode addMaterial_HMHMooneyRivlin(const int tape, const double alpha,
                                             const double beta,
                                             const double lambda,
                                             const double sigma_y);

  MoFEMErrorCode setBaseVolumeElementOps(
      const int tag, const bool do_rhs, const bool do_lhs,
      boost::shared_ptr<EpElement<VolumeElementForcesAndSourcesCore>> &fe);

  MoFEMErrorCode setGenericVolumeElementOps(
      const int tag, const bool add_elastic, const bool add_material,
      boost::shared_ptr<EpElement<VolumeElementForcesAndSourcesCore>> &fe_rhs,
      boost::shared_ptr<EpElement<VolumeElementForcesAndSourcesCore>> &fe_lhs);

  MoFEMErrorCode setGenericFaceElementOps(
      const bool add_elastic, const bool add_material,
      boost::shared_ptr<EpElement<FaceElementForcesAndSourcesCore>> &fe_rhs,
      boost::shared_ptr<EpElement<FaceElementForcesAndSourcesCore>> &fe_lhs);

  MoFEMErrorCode setElasticElementOps(const int tag);
  MoFEMErrorCode setElasticElementToTs(DM dm);

  MoFEMErrorCode setUpTSElastic(TS ts, Mat m, Vec f);
  MoFEMErrorCode solveElastic(TS ts, Vec x);

  MoFEMErrorCode postProcessResults(const int tag, const std::string file);
};

} // namespace EshelbianPlasticity

#endif //__ESHELBIAN_PLASTICITY_HPP__